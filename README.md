# Artificial Intelligence: Practice Sessions

Volgende instructies zijn voor Linux, maar zijn waarschijnlijk wel te volgen voor Windowsmensen.

## 1. Clone het labo

```
git clone https://gitlab.com/EAVISE/labo-ai-student
cd labo-ai-student
```

## 2. Installeer de dependencies


Voor Python versies 3.5 t.e.m. 3.7 is er een `requirements.txt` file met daarin alle dependencies.
Python 3.8 wordt niet ondersteund, en de enige oplossing momenteel is om te downgraden via je package manager naar keuze.
(Voor Arch users is er [Python 3.7](https://aur.archlinux.org/packages/python37/))

### Pip

Voor Pip gebruikers is het geen slecht idee om eerst een virtual environment aan te maken alvorens pakketjes te installeren.
```
pip3 install -r requirements.txt
```

### Conda

```
conda install --file requirements.txt
```

## 3. Open de notebook

```
jupyter notebook 
```

## 4. Keihard werken

Goed bezig! :)

## 5. Oplossing indienen

Mail de notebooks naar s.vandevelde@kuleuven.be, met naam "labo1\_voornaam\_achternaam.ipynb" en "labo2\_voornaam\_achternaam.ipynb"
